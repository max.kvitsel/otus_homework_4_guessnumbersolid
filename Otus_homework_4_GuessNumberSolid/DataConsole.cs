﻿using Otus_homework_4_GuessNumberSolid.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_homework_4_GuessNumberSolid
{
    public class DataConsole : IDataConsole
    {
        public int Read()
        {
            return int.Parse(Console.ReadLine());
        }

        public virtual void Write(string text) => Console.WriteLine(text);

        public void Clear()
        {
            Console.Clear();
        }

        public void NextLine()
        {
            Console.WriteLine("\r\n");
        }
    }
}
