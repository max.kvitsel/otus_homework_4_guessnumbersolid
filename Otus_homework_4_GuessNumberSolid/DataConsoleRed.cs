﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_homework_4_GuessNumberSolid
{
    public class DataConsoleRed : DataConsole
    {
        public override void Write(string text)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            base.Write(text);
            Console.ResetColor();
        }
    }
}
