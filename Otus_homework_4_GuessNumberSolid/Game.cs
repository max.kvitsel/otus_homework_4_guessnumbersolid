﻿using Otus_homework_4_GuessNumberSolid.Interfaces;
using System;

namespace Otus_homework_4_GuessNumberSolid
{
    public class Game
    {
        private readonly IDataConsole _dataConsole;
        private readonly IHiddenNumber _hiddenNumber;

        public Game(IDataConsole dataConsole, IHiddenNumber hiddenNumber)
        {
            this._dataConsole = dataConsole;
            this._hiddenNumber = hiddenNumber;
        }
        public void Execute()
        {
            this.SetOptions(out int tryAmount , out int begin , out int end , out int hiddenNumber);
            this.Start(tryAmount, begin, end, hiddenNumber);
        }

        private void SetOptions(out int tryAmount, out int begin, out int end, out int hiddenNumber)
        {

            this._dataConsole.Write("Please enter the number of attempts");
            tryAmount = this._dataConsole.Read();

            this._dataConsole.Write("Enter begin of range");
            begin = this._dataConsole.Read();

            this._dataConsole.Write("Enter end of range");
            end = this._dataConsole.Read();

            hiddenNumber = this._hiddenNumber.GetHiddenNumber(begin, end);
        }

        public void Start(int tryAmount, int begin, int end, int hiddenNumber)
        {
            bool isSuccess = false;

            Console.Clear();
            while (tryAmount > 0)
            {
                this._dataConsole.Write($"The game started!");
                this._dataConsole.Write($"Number of attempts: {tryAmount}");
                this._dataConsole.Write($"Enter a number between {begin} and {end}");
                int newUserValue = _dataConsole.Read();

                if (newUserValue == hiddenNumber)
                {
                    isSuccess = true;
                    break;
                }
                else
                {
                    if (newUserValue > hiddenNumber)
                        this._dataConsole.Write($"The hidden number is less.\r\n");
                    else
                        this._dataConsole.Write($"The hidden number is greater.\r\n");
                }
                tryAmount--;
            }

            if (isSuccess)
            {
                _dataConsole.Write("You win!\r\n");
                Console.ReadKey();
            }
            else
            {
                _dataConsole.Clear();
                _dataConsole.Write($"Game over. Hidden number was {hiddenNumber}.\r\n");
                Console.ReadKey();
            }
        }
    }
}
