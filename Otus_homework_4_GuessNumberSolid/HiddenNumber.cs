﻿using Otus_homework_4_GuessNumberSolid.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_homework_4_GuessNumberSolid
{
    internal class HiddenNumber : IHiddenNumber
    {
        public virtual int GetHiddenNumber(int begin, int end)
        {
            Random random = new Random();

            return random.Next(begin, end);
        }
    }
}
