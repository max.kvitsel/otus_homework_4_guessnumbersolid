﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_homework_4_GuessNumberSolid.Interfaces
{
    public interface IHiddenNumber
    {
        public int GetHiddenNumber(int begin, int end);
    }
}
