﻿using System;

namespace Otus_homework_4_GuessNumberSolid
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var dataConsole = new DataConsole();
            var hiddenNumber = new HiddenNumber();

            var game = new Game(dataConsole, hiddenNumber);
            while (true)
            {
                game.Execute();
            }
        }
    }
}
