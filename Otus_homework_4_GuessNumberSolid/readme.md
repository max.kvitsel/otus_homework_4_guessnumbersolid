﻿#### S: Single Responsibility Principle (Принцип единственной ответственности).
- Interfaces - только абстракции
- HiddenNumber - только "загадывает" число;
- DataConsole - отвечает за ввод/вывод;
- Game - игровая механика
- Program - точка входа;

#### O: Open-Closed Principle (Принцип открытости-закрытости).

- HiddenNumber (закрыт от модификаций, но доступен для расширений. Реализует интерфейс IHiddenNumber)

#### L: Liskov Substitution Principle (Принцип подстановки Барбары Лисков).
- Interfaces.DataConsoleRed() Поведение при переопределении методов не изменено. Изменился только функционал (цвет текста).

#### I: Interface Segregation Principle (Принцип разделения интерфейса).
- IDataReader и IDataWriter отвечают соответственно за ввод и вывод информации

#### D: Dependency Inversion Principle (Принцип инверсии зависимостей).
- Game -  зависит только от интерфейсов, а не от конкретной реализации